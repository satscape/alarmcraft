package info.satscape.alarmcraft.client;

import java.io.File;
import java.net.URL;

import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import info.satscape.alarmcraft.common.ModAlarmcraft;
import net.minecraft.client.Minecraft;
import net.minecraft.src.ModLoader;
import net.minecraftforge.client.event.sound.SoundLoadEvent;
import net.minecraftforge.event.ForgeSubscribe;

public class EventSounds {

	
	@ForgeSubscribe
	@SideOnly(Side.CLIENT)
    public void onSound(SoundLoadEvent event)
    {
        try 
        {
        	String[] sounds = {"carlock","alarmA","alarmB","alarmC","alarmD","alarmE","alarmF","alarmG","alarmH","alarmI"
        			,"alarmJ","alarmK","alarmL","alarmM"};
        	
        	for (int x = 0; x < sounds.length; x++) {
        		event.manager.soundPoolSounds.addSound("satscapealarmcraft:"+sounds[x]+".ogg"); 
        		ModAlarmcraft.log(sounds[x] +" audio file loaded");          
        	}
        }  
        catch (Exception e)
        {
            System.err.println("Failed to register one or more sounds.");
        }
    }
}