package info.satscape.alarmcraft.client;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import info.satscape.alarmcraft.common.AlarmSettings;
import info.satscape.alarmcraft.common.CommonProxy;
import info.satscape.alarmcraft.common.CommonTickHandler;
import info.satscape.alarmcraft.common.ModAlarmcraft;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.src.ModLoader;
import net.minecraft.world.World;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;


public class ClientProxy extends CommonProxy {

		
	    @Override
	    public void registerRenderInfo()
	    {


	    }
	    
	    @Override
	    public void registerMisc() {
	    	super.registerMisc();
			TickRegistry.registerTickHandler(new ClientTickHandler(), Side.CLIENT);
			
		}
	    
	    public void openAlarmGui(AlarmSettings alarm, EntityPlayer player) {
	    	GuiAlarmSettings ui=new GuiAlarmSettings(alarm, player);
	    	ModLoader.getMinecraftInstance().displayGuiScreen(ui);
		}
	    
	    
	 //   @Override
	//	public World getClientWorld() {
	//		return FMLClientHandler.instance().getClient().theWorld;
	//	}
	    
	    
}
