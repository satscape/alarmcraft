package info.satscape.alarmcraft.client;


import info.satscape.alarmcraft.common.ModAlarmcraft;
import info.satscape.alarmcraft.common.PacketHandler;
import java.util.EnumSet;
import java.util.Random;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.src.ModLoader;
import net.minecraftforge.common.Configuration;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.common.network.PacketDispatcher;

public class ClientTickHandler implements ITickHandler {

	Minecraft mc=ModLoader.getMinecraftInstance();
	GuiScreen hud = new GuiScreen();

	
	public void onTickInGame() {

		
		if (mc.currentScreen !=null) {


		}

	}
	
	public void onGui() {

			
			
	}
	
	
	public ClientTickHandler() {
	}

	@Override
	public void tickStart(EnumSet<TickType> type, Object... tickData) {
	}

	@Override
	public void tickEnd(EnumSet<TickType> type, Object... tickData) {
        if (type.equals(EnumSet.of(TickType.CLIENT)))
        {
                onTickInGame();
                
        }
        
        if (type.equals(EnumSet.of(TickType.WORLDLOAD)))
        {
        	System.out.println("WorldLoad event tick(client side)");
        }

        if (type.equals(EnumSet.of(TickType.RENDER))) {
        	onGui();
        }
	}

	@Override
	public EnumSet<TickType> ticks() {
		return EnumSet.of(TickType.CLIENT,TickType.WORLDLOAD,TickType.RENDER); 
	}

	@Override
	public String getLabel() {
		return "ClientTickHandler";
	}

}
