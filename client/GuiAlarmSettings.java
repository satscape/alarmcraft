package info.satscape.alarmcraft.client;

import info.satscape.alarmcraft.common.AlarmSettings;
import info.satscape.alarmcraft.common.ModAlarmcraft;
import info.satscape.alarmcraft.common.PacketHandler;

import java.util.ArrayList;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.src.ModLoader;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiAlarmSettings extends GuiScreen {
	private static final ResourceLocation myBackgroundTexture 
		= new ResourceLocation("satscapealarmcraft","textures/gui/guiBack.png");
	
	private EntityPlayer thePlayer;
	private AlarmSettings alarm;
	private int mouseCount=0;
	private int tempMeta=0;
	private String alertText="";
	
	public GuiAlarmSettings(AlarmSettings alarm,EntityPlayer player) {
		this.alarm=alarm;
		this.thePlayer=player;
	}

	public boolean doesGuiPauseGame() {
		return false;
	}

	@Override
	public void updateScreen() {
		// theGuiTextField1.updateCursorCounter();
	}

	@Override
	public void initGui() {
		Keyboard.enableRepeatEvents(true);
		showPage();
		Mouse.setGrabbed(false);
	}

	@Override
	public void drawScreen(int i, int j, float f) {
		
		if (mouseCount<10) {
			mouseCount++;
			Mouse.setGrabbed(false);
		}
		
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		//this.mc.renderEngine
		//		.bindTexture("/mods/SatscapeAlarmcraft/textures/gui/guiBack.png");
		this.mc.func_110434_K().func_110577_a(myBackgroundTexture);  // New in 1.6.2
		
		int posX = (this.width - 256) / 2;
		drawTexturedModalRect(posX, 5, 0, 0, 256, 230);

		drawCenteredString(fontRenderer, "Alarmcraft", width / 2, 10, 0xFF1010);
		drawCenteredString(fontRenderer, "Press the button below to cycle through", width/2, 30,0xFFFFFF);
		drawCenteredString(fontRenderer, "the different types of alarms available.", width/2, 40,0xFFFFFF);	
		drawCenteredString(fontRenderer, "When you've found the one you want,", width/2, 50,0xFFFFFF);	
		drawCenteredString(fontRenderer, "click the 'Set' button at the bottom.", width/2, 60,0xFFFFFF);
		drawCenteredString(fontRenderer, "Supply a redstone pulse (on/off signal)", width/2, 70,0xFFFFFF);
		drawCenteredString(fontRenderer, "to any side of the box to sound the alarm.", width/2, 80,0xFFFFFF);	
		drawCenteredString(fontRenderer,alertText,width/2,150,0x00FF00);
		
		// theGuiTextField1.drawTextBox();
		super.drawScreen(i, j, f);
	}

	private void showPage() {
		buttonList.clear();
		buttonList.add(new GuiButton(0, this.width/2-25, 210, 50, 20, "Set"));
		
		tempMeta=alarm.theType;
		char ch=(char) (65+tempMeta);
		
		if (tempMeta<26) {
			buttonList.add(new GuiButton(1, this.width/2-50,100,100,20,"Alarm "+ch));
		} else {
			buttonList.add(new GuiButton(1, this.width/2-50,100,100,20,"Bomb!"));
		}
		buttonList.add(new GuiButton(2, this.width/2-25,120,50,20,"Test"));

	}

	@Override
	protected void actionPerformed(GuiButton guibutton) {
		if (!guibutton.enabled) {
			return;
		}
		if (guibutton.id == 0) // SET alarm button
		{		
			AlarmSettings alarm=new AlarmSettings(tempMeta,false,this.alarm.x,this.alarm.y,this.alarm.z,"");
			PacketDispatcher.sendPacketToServer(PacketHandler.makeAlarmPacket(alarm, "update"));
			
			thePlayer.playSound("satscapealarmcraft:carlock", 1f,1f);
			
			
			mc.currentScreen = null;
			mc.setIngameFocus();
		
		} else if (guibutton.id==1) { //change alarm type button
			tempMeta++;
			if (tempMeta>12 && tempMeta<26) {
				tempMeta=26;
			} else if (tempMeta>26) {
				tempMeta=0;
			}
			char ch=(char) (65+tempMeta);
			if (tempMeta<26) {
				guibutton.displayString="Alarm "+ch;
				alertText="";
			} else {
				guibutton.displayString="Bomb!";
				alertText="*VERY* loud one-time-only 'alarm' :-)";
			}
		
		} else if (guibutton.id==2) { //test the alarm button
			char ch=(char) (65+tempMeta);
			if (tempMeta <26) {
				thePlayer.playSound("satscapealarmcraft:alarm"+ch,1f,1f);
				
				alertText="";
			} else {
				alertText="BOOM! - No test for this 'alarm' :-)";
			}
		}


	}

	@Override
	public void onGuiClosed() {
		Keyboard.enableRepeatEvents(false);
	}

	@Override
	protected void keyTyped(char c, int i) {
		if (i == 1) { // escape and dont save
			mc.currentScreen = null;
			mc.setIngameFocus();
			return;
		}
	}

	@Override
	protected void mouseClicked(int i, int j, int k) {
		// theGuiTextField1.mouseClicked(i, j, k);

		super.mouseClicked(i, j, k);
	}

}
