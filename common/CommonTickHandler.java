package info.satscape.alarmcraft.common;

import java.util.EnumSet;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

public class CommonTickHandler implements ITickHandler {

	//private World serverWorld=null;
	Long lastSecondTickAt=0l;
	Long lastMinuteTickAt=0l;
	
	
	public void onTickInGame()
	{	
		
    }
	

	
	////////////////////////////////////////////////
	public CommonTickHandler() {
	}

	@Override
	public void tickStart(EnumSet<TickType> type, Object... tickData) {
	}

	@Override
	public void tickEnd(EnumSet<TickType> type, Object... tickData) {

        if (type.equals(EnumSet.of(TickType.SERVER)))
        {
                onTickInGame();
                
        }
        

	}

	@Override
	public EnumSet<TickType> ticks() {
		return EnumSet.of(TickType.SERVER);  //add more types?
	}

	@Override
	public String getLabel() {
		return "CommonTickHandler";
	}

}
