package info.satscape.alarmcraft.common;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Icon;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockAlarm extends Block {
	
	private Icon icons[];
	
	public BlockAlarm(int par1) {
		super(par1, Material.iron);
        this.setCreativeTab(CreativeTabs.tabMisc);
        setBlockBounds(0.1875f,0.0f,0.1875f,  0.8125f,0.25f,0.8125f);
        this.setLightValue(0.2f);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IconRegister iconRegister) {
		icons=new Icon[2];
		icons[0] = iconRegister.registerIcon("satscapealarmcraft:alarmside");
		icons[1] = iconRegister.registerIcon("satscapealarmcraft:alarmtop");
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Icon getIcon(int side, int meta) {
		if (side==0 || side==1) {
			return icons[1];
		} else {
			return icons[0];
		}
	}
	
	
	@Override
	public void setBlockBoundsForItemRender()
	    {
		setBlockBounds(0.1875f,0.0f,0.1875f,  0.8125f,0.25f,0.8125f);
	    }
	
	public boolean renderAsNormalBlock()
    {
        return false;
    }
	
    public boolean isOpaqueCube()
    {
        return false;
    }
    
	  
	@Override
	public boolean canPlaceBlockAt(World theWorld, int x, int y, int z) {
		int id=theWorld.getBlockId(x,y-1,z);
		if (id>0 && id !=ModAlarmcraft.alarmBlock.blockID) {
			return true;
		} else {
			return false;
		}
	}

	public void onNeighborBlockChange(World theWorld, int x, int y, int z, int neighbourBlockId)
	    {
			ModAlarmcraft.log(x+","+y+","+z+" onNeighbourBlockChange()  remote="+theWorld.isRemote+"  -  "+theWorld.toString());
			
			if (!theWorld.isRemote) {
				AlarmSettings alarm=new AlarmSettings(x,y,z,"");
		    	alarm.load();
		    	
		        if (alarm.isPowered && !theWorld.isBlockIndirectlyGettingPowered(alarm.x,alarm.y,alarm.z))
		        {
		        	alarm.isPowered=false;
		        	alarm.save();
		        	
		        }
		        else if (!alarm.isPowered && theWorld.isBlockIndirectlyGettingPowered(alarm.x,alarm.y,alarm.z))
		        {
		        	alarm.isPowered=true;
		        	alarm.save();
		        	
		        	if (alarm.theType==26) {  //   it's a b...    *airplane reference :-)
		        		EntityPlayer nplayer=theWorld.getClosestPlayer(x,y,z,-1);
		        		theWorld.createExplosion (nplayer, alarm.x,alarm.y,alarm.z, 10, true); //server side explosion
		        	}
		        	//send packet to client so it can make the sound effects
		        	PacketDispatcher.sendPacketToAllAround(alarm.x, alarm.y, alarm.z, 50, theWorld.provider.dimensionId,
		        			PacketHandler.makeAlarmPacket(alarm, "poweredon"));
		        	
		        }
			}
	      
	    }


	
	@Override
	public boolean onBlockActivated(World world, int i, int j, int k,
			EntityPlayer thePlayer, int par6, float par7,float par8, float par9) {
		
			if (!world.isRemote) { //SERVER SIDE
		    	
		    	ModAlarmcraft.log("Server side onBlockActivated");
				AlarmSettings alarm=new AlarmSettings(i,j,k, thePlayer.username);
				alarm.load();
				PacketDispatcher.sendPacketToAllAround(i, j, k, 20,thePlayer.dimension, 
						PacketHandler.makeAlarmPacket(alarm,"info"));
		    	
				
		    	
			}
			world.playSoundEffect(i,j,k,"satscapealarmcraft:carlock",1f,1f);
			
        return true;
    }

	@Override
	public void onBlockDestroyedByPlayer(World par1World, int par2, int par3,
			int par4, int par5) {
		// TODO Auto-generated method stub
		super.onBlockDestroyedByPlayer(par1World, par2, par3, par4, par5);
	}

	@Override
	public void onBlockDestroyedByExplosion(World par1World, int par2,
			int par3, int par4, Explosion par5Explosion) {
		// TODO Auto-generated method stub
		super.onBlockDestroyedByExplosion(par1World, par2, par3, par4, par5Explosion);
	}

	
	
    
    
}
