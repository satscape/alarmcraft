package info.satscape.alarmcraft.common;

import info.satscape.alarmcraft.client.GuiAlarmSettings;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.src.ModLoader;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;

public class PacketHandler implements IPacketHandler {

	public PacketHandler() {
	}

	@Override
	public void onPacketData(INetworkManager manager,Packet250CustomPayload packet, Player player) {
		
		if (packet.channel.equals("AlarmcraftMain")) {
			DataInputStream inputStream = new DataInputStream(new ByteArrayInputStream(packet.data));
			
			try {
				AlarmSettings alarm=new AlarmSettings(inputStream.readInt(),inputStream.readBoolean(),
						inputStream.readInt(), inputStream.readInt(), inputStream.readInt(), inputStream.readUTF());
				String msgType=inputStream.readUTF();

				ModAlarmcraft.log(msgType+" packet received");
				
				if (msgType.contentEquals("info")) {     // server > client		
					EntityPlayer pl=(EntityPlayer)player;
					if (pl.username.contentEquals(alarm.playersUsername)) {
						ModAlarmcraft.proxy.openAlarmGui(alarm, pl);
					}
			    	
				} else if (msgType.contentEquals("update")) {   // client > server
					alarm.save();
				
					
				} else if (msgType.contentEquals("poweredon")) {// server > client
					EntityPlayer nplayer=(EntityPlayer) player; // this goes to all players in range
					
					if (alarm.theType<26) {
	            		char ch=(char) (65+alarm.theType);
	            		if (nplayer !=null) {
	            			nplayer.playSound("satscapealarmcraft:alarm"+ch, 1f, 1f);
	            		}
	            		//ModLoader.getMinecraftInstance().theWorld.playSoundEffect(alarm.x,alarm.y,alarm.z,"Alarm.alarm"+ch,1f,1f);

	            	} else {
      		
	            		if (nplayer !=null) { // tree falls in a forest... or bomb explodes and theres no one to witness it.... :-)
	            			//nplayer.worldObj.createExplosion(nplayer, alarm.x,alarm.y,alarm.z, 10, true);
	            			//ModLoader.getMinecraftInstance().theWorld.playSoundEffect(alarm.x,alarm.y,alarm.z, "random.explode", 4f, 1f);
	            			nplayer.playSound("random.explode", 4f, 1f);
	            		}
	            	}
				}

				
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		}
	}
	

	public static Packet250CustomPayload makeAlarmPacket(AlarmSettings alarm, String msgType) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream(8);
		DataOutputStream outputStream = new DataOutputStream(bos);
		try {
			outputStream.writeInt(alarm.theType);
			outputStream.writeBoolean(alarm.isPowered);
			outputStream.writeInt(alarm.x);
			outputStream.writeInt(alarm.y);
			outputStream.writeInt(alarm.z);
			outputStream.writeUTF(alarm.playersUsername);
			outputStream.writeUTF(msgType);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		Packet250CustomPayload packet = new Packet250CustomPayload();
		packet.channel = "AlarmcraftMain";
		packet.data = bos.toByteArray();
		packet.length = bos.size();
		return packet;
	}
}
