package info.satscape.alarmcraft.common;

import info.satscape.alarmcraft.client.EventSounds;
import java.io.File;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.src.ModLoader;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;

/// Alarmcraft Mod by Scott Hather AKA "Satscape"
/// v1.0.0 - 20th June 2013


@Mod(modid="satscapealarmcraft", name="Alarmcraft", version="1.0.0", dependencies="required-after:Forge@[7.8,)")
@NetworkMod(clientSideRequired = true, serverSideRequired = false,
channels={"AlarmcraftMain"}, packetHandler = PacketHandler.class)


public class ModAlarmcraft {
public static String version="1.0.0";
	
	@SidedProxy(clientSide = "info.satscape.alarmcraft.client.ClientProxy",
			serverSide = "info.satscape.alarmcraft.common.CommonProxy")
	public static CommonProxy proxy;
	
	//@Instance
	//public static ModAlarmcraft instance;
	
	
	/** Block instances and IDs */
    static int alarmBlockId = 0;
    static Block alarmBlock;

    ////config file settings
    public static Configuration config;

 
	@PreInit
	public void preInit(FMLPreInitializationEvent evt) {
		config = new Configuration(evt.getSuggestedConfigurationFile());
		
		try {	
			config.load();
			
			//Block IDs from the config file
			alarmBlockId=config.getBlock("AlarmBlock",3220).getInt();
			
			
		} catch(Exception e) {
			log("Could not allocate ID - "+e.getMessage());
			e.printStackTrace();
		} finally {
			config.save();
		}
		

		MinecraftForge.EVENT_BUS.register(new EventSounds());   

		
	}

	
	@Init
    public void initLoad(FMLInitializationEvent event) {
		alarmBlock = new BlockAlarm(alarmBlockId).
				setStepSound(Block.soundMetalFootstep).setHardness(2F).setResistance(1.0F).setUnlocalizedName("SatscapeAlarm");
		
		GameRegistry.registerBlock(alarmBlock,"SatscapeAlarm");
		
		LanguageRegistry.addName(alarmBlock, "Alarm box");

		
		GameRegistry.addRecipe(new ItemStack(alarmBlock, 1), new Object[]{
            "III", "IRI", "ILI",
            Character.valueOf('I'), Item.ingotIron,
            Character.valueOf('R'), Item.redstone,
            Character.valueOf('L'), Block.lever
        });
	
		
		proxy.registerRenderInfo();
		proxy.registerMisc();

    }
	
	/** my logger - for some reason I can't get the regular java Logger to work :-( */
	public static void log(String logText) {
		//TODO: turn this off for release versions
		System.out.println("[Alarmcraft] "+logText); 
		//sendChat("LOG:"+logText);
	}
	
	
	/** returns true when it is daytime in the OVERWORLD, ignores other world times */
	public static boolean isDayTime() {
		return MinecraftServer.getServer().worldServers[0].isDaytime();
	}
	
	/** gets the .minecraft/saves/CURRENTWORLD/alarmcraft/   called from server only, client never calls it */
	public static String getSavesDataFolder() {
		
		Side side = cpw.mods.fml.common.FMLCommonHandler.instance().getEffectiveSide();
		if (side.isClient()) { return ""; }
		 
		String strmc=new File(".").getAbsolutePath();
		strmc=strmc.substring(0,strmc.length()-1); //remove dot on end!
		
		File test=new File(strmc+"saves");  // present on Integrated server, not on dedicated server
		String ret="";
		
		if (test.exists()) { /// Integrated server
			String worldname=ModLoader.getMinecraftInstance().getIntegratedServer().getFolderName();
			ret= new File(Minecraft.getMinecraft().mcDataDir.getAbsoluteFile()+File.separator+"saves"
        		+ File.separator + worldname + File.separator + "alarmcraft" + File.separator).getAbsolutePath()
        		+ File.separator;
			
		} else { // Dedicated server
			String worldname=ModLoader.getMinecraftServerInstance().getFolderName();	
			strmc=strmc+worldname+File.separator+"alarmcraft"+File.separator;
			ret=new File(strmc).getAbsolutePath()+ File.separator;
		}

		File f=new File(ret);
		if (!f.exists()) {
			f.mkdirs();
		}
		
		//ModAlarmcraft.log("getSavesDataFolder = "+ret);
		return ret;

	}
	

}




