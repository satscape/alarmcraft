package info.satscape.alarmcraft.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

import cpw.mods.fml.relauncher.Side;

public class AlarmSettings {

	public int theType=0;
	public boolean isPowered=false;
	public int x,y,z;
	public String playersUsername="";  //this is used to remember who right clicked the box so when the packet arrives it opens the UI on the correct users screen
	
	/** constructs the settings for the alarm at the specified coords */
	public AlarmSettings(int type,boolean powered,int x, int y, int z, String user) {
		this.theType=type;
		this.isPowered=powered;
		this.x=x;
		this.y=y;
		this.z=z;
		this.playersUsername=user;
	}
	
	/** constructs the alarm settings or default if none exists - server can then call load() method */
	public AlarmSettings(int x,int y, int z, String user) {
		this.x=x;
		this.y=y;
		this.z=z;
		this.playersUsername=user;
	}
	

	public void save() {
		Side side = cpw.mods.fml.common.FMLCommonHandler.instance().getEffectiveSide();
		if (side.isClient()) { ModAlarmcraft.log("CLIENT save() refused"); return; }
		
		String fullpath=ModAlarmcraft.getSavesDataFolder()+ "A"+ x +"_"+y+"_"+z+".ac";
		
		File f=new File(fullpath);
	    PrintWriter out;
	    try {
	    	out = new PrintWriter(new FileWriter(f.getAbsolutePath()));
	        out.println(this.theType);
	        out.println(this.isPowered);
	        out.println(this.playersUsername);
	        out.close();
	        
	        } catch (Exception ex) {
	        	ex.printStackTrace();
	        }
	}
	

	public void load() {
		Side side = cpw.mods.fml.common.FMLCommonHandler.instance().getEffectiveSide();
		if (side.isClient()) { return; }
		
        FileInputStream in= null;
        String inputLine="";
        String fullpath=ModAlarmcraft.getSavesDataFolder()+ "A"+ x +"_"+y+"_"+z+".ac";
        
        File f=new File(fullpath);
        if (!f.exists()) {
        	this.theType=0;
        	this.isPowered=false;
        	return;
        }
        
        try {
        	BufferedReader br = new BufferedReader(new FileReader(fullpath));
            this.theType = Integer.parseInt(br.readLine());
            this.isPowered=Boolean.parseBoolean(br.readLine());
            //this.playersUsername=br.readLine().trim();
            br.close();
           
        } catch(Exception e) {e.printStackTrace();}
	}
	
}
